# meals_app

### Tema Aplikasi :
Menampilkan resep masakan dari chef mr stein.

### Detail Teknis :
- Menggunakan bloc state management untuk home, list dan menu data yang didapatkan dari API TheMealDB<br/>
- Referensi API dari <a href="https://www.themealdb.com/api.php">TheMealDB</a><br/>
- Register dan Login menggunakan Firebase dengan Auth Email dan Passowrd<br/>
- Support digunakan di flutter versi web maupun app
- Penambahan SplashScreen dan Onboarding Page

### Untuk Memulai :
1. lakukan clone dari link repository, pastikan tidak ada masalah dengan perintah flutter doctor
2. lakukan perintah flutter clean, flutter pub get , flutter pub upgrade
3. untuk build flutter web dengan perintah flutter run
4. untuk build flutter app android dengan perintah flutter build apk

### APK : 
<a id="raw-url" href="https://gitlab.com/chubee12002/final-project-flutter-sc/-/tree/master/apk">Download FILE</a>

### HomePage :
 - Halaman yang menampilkan list category serta makanan resep italia serta pilihan untuk melihat makanan di negara lain<br/><br/>![](images/home.jpeg)
 
### Detail Page :
 - Halaman  yang berguna untuk melihat detail resep makan mulai dari cara membuat dan bahan-bahannya<br/><br/> ![](images/detail.jpeg)

### Login Page : 
 - Halaman untuk melakukan login <br/><br/>![](images/login.jpeg)

### Register Page :
 - Halaman untuk melakukan register <br/><br/>![](images/signup.jpeg)

### Drawer Page :
 - Halaman untuk melihat menu aplikasi <br/><br/>![](images/drawer.jpeg)

### About Page :
 - Halaman yang menampilkan biodata, deskripsi dan tujuan aplikasi, serta tools yang digunakan<br/><br/>![](images/about.jpeg)

### Web Page Version:
 ![](images/webpage.jpeg)
