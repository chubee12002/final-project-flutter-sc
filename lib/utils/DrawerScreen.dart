import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meals_app/ui/about_page.dart';
import 'package:meals_app/ui/home/home_page.dart';
import 'package:meals_app/ui/login_page.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {

  Future<void>_signOut()async{
    FirebaseAuth.instance.signOut();
  }
  @override
  Widget build(BuildContext context) {
    FirebaseAuth auth = FirebaseAuth.instance;
    var emailTeks = "";

    if ( auth.currentUser != null ){
      emailTeks=auth.currentUser.email;
    }

    return Drawer(
        child: ListView(
      children: <Widget>[
        new UserAccountsDrawerHeader(
          accountName: new Text("Name User"),
          accountEmail: new Text(emailTeks),
          currentAccountPicture:
              CircleAvatar(backgroundImage: AssetImage("images/userprofile.png")),
          onDetailsPressed: () {},
        ),
        new ListTile(
          title: new Text("Home"),
          leading: new Icon(Icons.home),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context2) => HomePage(),
              ));
          },
        ),
        new ListTile(
          title: new Text("About Me"),
          leading: new Icon(Icons.info),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context2) => AboutPage(),
              ));
          },
        ),
        new ListTile(
          title: new Text("Logout"),
          leading: new Icon(Icons.logout),
          onTap: () {
            _signOut().then((value)=> Navigator.of(context).pushReplacement(
              CupertinoPageRoute(builder: (context) => LoginPage()))
            );
          },
        )
      ],
    ));
  }
}
