import 'package:flutter/material.dart';
import 'package:meals_app/utils/DrawerScreen.dart';

class AboutPage extends StatefulWidget {
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  var tools = [
    {"nama": "Visual Studio", "icon": "assets/visual.png"},
    {"nama": "Postman", "icon": "assets/postman.png"}
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        title: Image.asset(
          "assets/icon.png",
          width: 190,
          height: 190,
        ),
      ),
      drawer: DrawerScreen(),
      body: ListView(
        children: <Widget>[
          _buildCover(),
          SizedBox(
            height: 8,
          ),
          Container(
            padding: EdgeInsets.all(5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Tentang Aplikasi",
                  style: TextStyle(
                      fontFamily: "Roboto", fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 4,
                ),
                Text(
                  "Aplikasi ini adalah aplikasi yang bertemakan tentang resep makanan di dalamnya berisikan kategori makanan ,tata cara pembuat dan bahan makanan untuk membuat masakan tersebut ",
                  style: TextStyle(fontSize: 12, fontFamily: "Montserrat"),
                ),
                SizedBox(
                  height: 12,
                ),
                Text(
                  "Tujuan",
                  style: TextStyle(
                      fontFamily: "Roboto", fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 4,
                ),
                Text(
                  "Tujuan dibuatnya aplikasi ini adalah untuk memenuhi syarat lulus training Flutter di Sanbercode batch 23 ",
                  style: TextStyle(fontSize: 12, fontFamily: "Montserrat"),
                ),
                SizedBox(
                  height: 12,
                ),
                Text(
                  "Tools yang dipakai",
                  style: TextStyle(
                      fontFamily: "Roboto", fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 4,
          ),
          _buildTool()
        ],
      ),
    );
  }

  Widget _buildTool() {
    return ListView.builder(
        itemCount: tools.length,
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return ListTile(
            leading: Image.asset(
              tools[index]["icon"],
              height: 30,
              width: 30,
            ),
            title: Text(
              tools[index]["nama"],
              style: TextStyle(fontFamily: "Roboto"),
            ),
          );
        });
  }

  Widget _buildCover() {
    return Container(
      color: Colors.red,
      width: double.infinity,
      height: 240,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 80,
            height: 80,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(12)),
              image: DecorationImage(
                  image: AssetImage("images/tomytjoen.jpg"), fit: BoxFit.cover),
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            "Tomy Dwi Dayanto H.",
            style: TextStyle(
                color: Colors.white,
                fontFamily: "Roboto",
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 4,
          ),
          Text(
            "A Programmer who wants to share every steps",
            style: TextStyle(
                fontSize: 12, color: Colors.white, fontFamily: "Montserrat"),
          ),
          SizedBox(
            height: 4,
          ),
          Text(
            "chubee12002@gmail.com",
            style: TextStyle(
                fontSize: 12, color: Colors.white, fontFamily: "Montserrat"),
          ),
        ],
      ),
    );
  }
}
