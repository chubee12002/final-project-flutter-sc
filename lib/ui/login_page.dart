import 'package:firebase_auth/firebase_auth.dart';
//import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meals_app/ui/home/home_page.dart';
import 'package:meals_app/ui/signup_page.dart';
import 'package:meals_app/utils/others.dart';


class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginPageState();
  }
}

class LoginPageState extends State<LoginPage> {
  bool acceptterms = false;
  bool isloading = false;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController edtemail = TextEditingController(text: "");
  TextEditingController edtpsw = TextEditingController(text: "");
  final _formKey = GlobalKey<FormState>();

  FirebaseAuth _firebaseAuth=FirebaseAuth.instance;

  @override
  void initState() {
    super.initState();
  }

  setSnackbar(String msg) {
    ScaffoldMessenger.of(context).showSnackBar(new SnackBar(content: new Text(msg)));
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Image.asset(
        "images/bglogin2.jpeg",
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        fit: BoxFit.cover,
      ),
      Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.transparent,
        body: Column(
          children: [
            Expanded(
              child: Center(
                child: SingleChildScrollView(
                  padding:
                      EdgeInsets.only(left: 20, right: 20, top: kToolbarHeight),
                  child: Form(
                    key: _formKey,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          RichText(
                            textAlign: TextAlign.start,
                            text: TextSpan(
                              style: Theme.of(context)
                                  .textTheme
                                  .headline5
                                  .merge(TextStyle(color: Colors.yellow)),
                              text: "Welcome ",
                              children: <TextSpan>[
                                TextSpan(
                                    text: "Back,",
                                    style: TextStyle(
                                        color: Colors.yellow,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                          ),
                          Text(
                            'Sign in to continue',
                            style: TextStyle(color: Colors.white),
                          ),
                          SizedBox(height: 30),
                          Text('Login',
                              style:
                                  Theme.of(context).textTheme.headline5.merge(
                                        TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                      )),
                          Container(
                            width: MediaQuery.of(context).size.width / 2,
                            color: Colors.yellow,
                            height: 2,
                          ),
                          Container(
                            decoration:
                                DesignConfig.boxDecorationContainerBorder(
                                    Colors.white, 10),
                            margin: EdgeInsets.only(top: 20),
                            padding:
                                EdgeInsets.only(left: 10, top: 5, bottom: 5),
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              cursorColor: Colors.black,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.email,
                                  color: Colors.white,
                                ),
                                hintText: 'Email',
                                hintStyle: Theme.of(context)
                                    .textTheme
                                    .subtitle2
                                    .merge(TextStyle(color: Colors.white)),
                                border: InputBorder.none,
                              ),
                              keyboardType: TextInputType.emailAddress,
                              validator: (val) => Constant.validateEmail(val),
                              controller: edtemail,
                            ),
                          ),
                          Container(
                            decoration:
                                DesignConfig.boxDecorationContainerBorder(
                                    Colors.white, 10),
                            margin: EdgeInsets.only(top: 20, bottom: 15),
                            padding:
                                EdgeInsets.only(left: 10, top: 5, bottom: 5),
                            child: TextFormField(
                              style: TextStyle(color: Colors.white),
                              cursorColor: Colors.black,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.lock,
                                  color: Colors.white,
                                ),
                                hintText: 'Password',
                                hintStyle: Theme.of(context)
                                    .textTheme
                                    .subtitle2
                                    .merge(TextStyle(color: Colors.white)),
                                border: InputBorder.none,
                              ),
                              obscureText: true,
                              validator: (val) =>
                                  val.trim().isEmpty ? 'Enter Password' : null,
                              controller: edtpsw,
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: GestureDetector(
                              onTap: () {
                                setSnackbar('Lupa password');
                              },
                              child: Text(
                                'Lupa Password?',
                                textAlign: TextAlign.end,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                          if (isloading)
                            Center(
                                child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: new CircularProgressIndicator(),
                            )),
                          GestureDetector(
                            onTap: () async {
                              if (_formKey.currentState.validate()) {
                                setSnackbar('Loading');
                                await _firebaseAuth.signInWithEmailAndPassword(
                                  email: edtemail.text, 
                                  password: edtpsw.text).
                                  then((value) => Navigator.of(context).pushReplacement(
                                    CupertinoPageRoute(builder: (context) => HomePage()))
                                  ).catchError((onError) => {

                                    setSnackbar('Wrong Account Credentials or no found account')

                                      //Handle error i.e display notification or toast
                                  });
                                //await _firebaseAuth.createUserWithEmailAndPassword(email: edtemail.text,password: edtpsw.text);
                                //Navigator.of(context).pushReplacement(CupertinoPageRoute(builder: (context) => CategoryActivity()));
                              }
                            },
                            child: Container(
                              width: double.maxFinite,
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(top: 40),
                              padding: EdgeInsets.symmetric(vertical: 18),
                              child: Text(
                                'Login',
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1
                                    .merge(TextStyle(
                                        color: Colors.blueGrey,
                                        fontWeight: FontWeight.bold)),
                              ),
                              decoration:
                                  DesignConfig.boxDecorationContainerColor(
                                      Colors.white, 15),
                            ),
                          ),
                        ]),
                  ),
                ),
              ),
            ),
            bottomWidget(),
          ],
        ),
      ),
    ]);
  }

  Widget bottomWidget() {
    return Column(mainAxisSize: MainAxisSize.min, children: [
      Row(children: [
        Expanded(
            child: Divider(
          color: Colors.white,
          endIndent: 10,
        )),
        Text(
          "Or",
          style: TextStyle(color: Colors.white),
        ),
        Expanded(
            child: Divider(
          color: Colors.white,
          indent: 10,
        )),
      ]),
      GestureDetector(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => SignupPage()));
        },
        child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                style: Theme.of(context)
                    .textTheme
                    .button
                    .merge(TextStyle(color: Colors.white)),
                text: 'Don\'t have account ?',
                children: <TextSpan>[
                  TextSpan(
                      text: 'signup',
                      style: Theme.of(context).textTheme.button.merge(TextStyle(
                            decoration: TextDecoration.underline,
                            color: Colors.white,
                          ))),
                ])),
      ),
      SizedBox(height: 10),
    ]);
  }
}
