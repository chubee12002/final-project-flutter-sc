import 'package:flutter/foundation.dart';
import 'package:meals_app/model/menu.dart';
import 'package:http/http.dart' as http;
import 'package:meals_app/utils/server.dart';

abstract class MenuRepository {
  Future<List<Menu>> getMenu(String nation);
  Future<List<Menu>> getMenyByCategory(String category);
  Future<List<Menu>> getMenuByArea(String area);
  Future<List<Menu>> getSearchMenu(String keyword);
  Future<String> getMenuRecipe(String id);
}

class MenuRepositoryImp extends MenuRepository {
  @override
  Future<List<Menu>> getMenu(String nation) async {
    var response = await http.get(Uri.parse(Server.url+ "/filter.php?a=" + nation));
    //var response = await http.get(Uri.https(Server.url, "/filter.php?a=" + nation));
    //var response = await http.get(Server.url+"/filter.php?a=" + nation);
    //print("respon bodi "+response.body);
    if (response.statusCode == 200) {
      return compute(listMenuFromJson, response.body);
    } else {
      throw Exception();
    }
    // ignore: todo
    // TODO: implement getMenu
  }

  @override
  Future<List<Menu>> getMenyByCategory(String category) async {
    var response = await http.get(Uri.parse(Server.url + "/filter.php?c=" + category));
    //var response = await http.get(Uri.https(Server.url , "/filter.php?c=" + category));
    //print("respon bodi "+response.body);
    if (response.statusCode == 200) {
      return compute(listMenuFromJson, response.body);
    } else {
      throw Exception();
    }
    // ignore: todo
    // TODO: implement getMenyByCategory
  }

  @override
  Future<List<Menu>> getSearchMenu(String keyword) async {
    var response = await http.get(Uri.parse(Server.url + "/search.php?s=" + keyword));
    //var response = await http.get(Uri.https(Server.url , "/search.php?s=" + keyword));
    //print("respon bodi "+response.body);
    if (response.statusCode == 200) {
      print(response.body);
      return compute(listMenuFromJson, response.body);
    } else {
      throw Exception();
    }
    // ignore: todo
    // TODO: implement getSearchMenu
  }

  @override
  Future<String> getMenuRecipe(String id) async {
    var response = await http.get(Uri.parse(Server.url + "/lookup.php?i=" + id));
    //var response = await http.get(Uri.https(Server.url , "/lookup.php?i=" + id));
    //print("respon bodi "+response.body);
    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw Exception();
    }
    // ignore: todo
    // TODO: implement getMenuRecipe
  }

  @override
  Future<List<Menu>> getMenuByArea(String area) async {
    var response = await http.get(Uri.parse(Server.url + "/filter.php?a=" + area));
    //var response = await http.get(Uri.https(Server.url , "/filter.php?a=" + area));
    //print("respon bodi "+response.body);
    if (response.statusCode == 200) {
      return compute(listMenuFromJson, response.body);
    } else {
      throw Exception();
    }
    // ignore: todo
    // TODO: implement getMenuByArea
  }
}
