import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meals_app/bloc/home/home_bloc.dart';
import 'package:meals_app/bloc/list_menu_by_area/list_menu_by_area_bloc.dart';
import 'package:meals_app/bloc/list_menu_by_category/list_menu_by_category_bloc.dart';
import 'package:meals_app/bloc/menu_recipe/menu_recipe_bloc.dart';
import 'package:meals_app/bloc/search_menu/search_menu_bloc.dart';
import 'package:meals_app/resource/category_resource.dart';
import 'package:meals_app/resource/menu_repository.dart';
//import 'package:meals_app/ui/home/home_page.dart';
import 'package:meals_app/ui/splash_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<HomeBloc>(
          create: (context) => HomeBloc(
              categoryRepository: CategoryRepositoryImp(),
              menuRepository: MenuRepositoryImp()),
        ),
        BlocProvider<ListMenuByCategoryBloc>(
          create: (context) =>
              ListMenuByCategoryBloc(menuRepository: MenuRepositoryImp()),
        ),
        BlocProvider<SearchMenuBloc>(
          create: (context) =>
              SearchMenuBloc(menuRepository: MenuRepositoryImp()),
        ),
        BlocProvider<MenuRecipeBloc>(
          create: (context) =>
              MenuRecipeBloc(menuRepository: MenuRepositoryImp()),
        ),
        BlocProvider<ListMenuByAreaBloc>(
          create: (context) =>
              ListMenuByAreaBloc(menuRepository: MenuRepositoryImp()),
        ),
      ], child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.red,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: SplashScreen(),
      )
    );
  }
}